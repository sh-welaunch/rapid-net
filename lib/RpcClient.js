const dgram = require('dgram')
const EventEmitter = require('events')
const uuid = require('uuid/v4')

class RpcClient extends EventEmitter {
  constructor() {
    super()
    this.client = dgram.createSocket('udp4')
  }

  connect(host, port) {
    this.host = host
    this.port = port
  }

  invoke(method, params, options) {
    params = params || {}
    options = options || {}
    let id = options.id || uuid()

    let message = JSON.stringify({method: method, params: params, id: id})
    var buffer = Buffer.from(message)
    this.client.send(buffer, this.port, this.host)
  }
}

module.exports = RpcClient
