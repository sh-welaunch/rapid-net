const dgram = require('dgram')
const EventEmitter = require('events')

class RpcServer extends EventEmitter {
  constructor() {
    super()
    this.server = dgram.createSocket('udp4')
    this.invokes = {}
  }

  listen(port) {
    this.server.bind(port)
    this.server.on('message', (buffer, remote) => {
      let invoke = JSON.parse(buffer.toString())
      if (!this.invokes[invoke.id]) {
        this.invokes[invoke.id] = true
        this.emit(invoke.method, invoke.params, () => {})
      }
    })
  }

  close() {
    this.server.close()
  }
}

module.exports = RpcServer
