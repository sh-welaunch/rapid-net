const uuid = require('uuid/v4')
const EventEmitter = require('events')
const zlib = require('zlib')
const { StringDecoder } = require('string_decoder')

const TcpClient = require('./TcpClient')
const TlsClient = require('./TlsClient')

class HttpClient extends EventEmitter {
  constructor(url) {
    super()
    this.id = uuid()
  }

  connect(host, tls, cb) {
    this.client = tls ? new TlsClient() : new TcpClient()
    this.cbs = []
    this.buffer = new Buffer([])
    this.response = null
    this.client.on('data', this._tlsClientOnData.bind(this))
    this.host = host
    this.client.connect(host, tls ? 443 : 80, cb)
  }

  request(options, cb) {
    options = options || {}
    cb = cb || function(){}
    let method = options.method || 'GET'
    let path = options.path || '/'
    let headers = options.headers || {}
    let body = options.body || ''

    let request = `${method} ${path} HTTP/1.1\r\n`
    request += `Host:${this.host}\r\n`
    request += `Accept-Encoding:gzip\r\n`
    for (let key in headers) {
      request += `${key}:${headers[key]}\r\n`
    }
    if (body) {
      request += `Content-Length:${body.length}\r\n`
    }
    request += `\r\n`
    if (body) {
      request += body
    }
    //console.log(request)
    this.cbs.push(cb)
    this.client.write(request)
    //console.log('request sent', new Date())
  }

  close() {
    this.client.close()
  }

  _tlsClientOnData(buffer) {
    //console.log('data received', buffer.length)
    this.buffer = Buffer.concat([this.buffer, buffer])

    if (!this.response) {
      this.response = {}
      let headers = buffer.toString().split('\r\n\r\n', 1)[0]
      this.buffer = this.buffer.slice(headers.length + 4)
      headers = headers.split('\r\n')
      this.response.status = parseInt(headers[0].split(' ')[1])
      this.response.headers = {}
      for (let i = 1; i < headers.length; i++) {
        let key = headers[i].substr(0, headers[i].indexOf(':')).trim().toLowerCase()
        let value = headers[i].substr(headers[i].indexOf(' ') + 1).trim().toLowerCase()
        this.response.headers[key] = value
      }
      if (this.response.headers['content-length']) {
        this.response.contentLength = parseInt(this.response.headers['content-length'])
      } else if (this.response.headers['transfer-encoding'] == 'chunked') {
        this.response.chunked = true
      } else {
        this.response.contentLength = 0
      }
    }

    if (this.response.chunked) {
      if (!this.response.chunks) {
        this.response.chunks = []
      }
      while (true) {
        if (!this.response.chunkLength) {
          this.response.chunkLength = this._consumeLine()
          if (this.response.chunkLength == null) {
            break
          }
          this.response.chunkLength = parseInt(this.response.chunkLength, 16)
        }

        if (this.response.chunkLength == 0) {
          this.response.chunkLength = null
          this.response.body = Buffer.concat(this.response.chunks)
          break
        } else {
          let chunk = this._consumeLength(this.response.chunkLength)
          if (!chunk) {
            break
          }
          this.response.chunkLength = null
          this.response.chunks.push(chunk)
          this._consumeLine()
        }
      }
    } else if (this.buffer.length >= this.response.contentLength) {
      this.response.body = this._consumeLength(this.response.contentLength)
    }

    if (this.response && this.response.body) {
      if (this.response.headers['content-encoding'] == 'gzip') {
        this.response.body = zlib.unzipSync(this.response.body)
      }
      this.response.body = new StringDecoder().end(this.response.body)
      if (this.response.headers['content-type'] == 'application/json') {
        this.response.body = JSON.parse(this.response.body)
      }
      this.cbs.shift()(null, this.response)
      this.emit('response', this.response)
      this.response = null
    }
  }

  _consumeLine() {
    for (let i = 0; i < this.buffer.length; i++) {
      if (this.buffer[i] == 0x0a) {
        let line = this.buffer.toString('ascii', 0, i - 1)
        this.buffer = this.buffer.slice(i + 1)
        return line
      }
    }
    return null
  }

  _consumeLength(length) {
    if (this.buffer.length < length) {
      return null
    }
    let chunk = this.buffer.slice(0, length)
    this.buffer = this.buffer.slice(length)
    return chunk
  }
}

module.exports = HttpClient
