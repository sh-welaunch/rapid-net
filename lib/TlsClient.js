const tls = require('tls')
const EventEmitter = require('events')

class TlsClient extends EventEmitter {
  constructor() {
    super()
  }

  connect(host, port, cb) {
    cb = cb || function(){}

    this.client = tls.connect({host: host, port: port}, () => {
      cb(null)
    })
    this.client.on('data', (buffer) => {
      this.emit('data', buffer)
    })
    this.client.on('error', () => {})
  }

  write(buffer) {
    this.client.write(buffer)
  }

  close() {
    this.client.destroy()
  }
}

module.exports = TlsClient
