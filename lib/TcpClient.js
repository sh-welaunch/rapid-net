const net = require('net')
const EventEmitter = require('events')

class TcpClient extends EventEmitter {
  constructor() {
    super()
  }

  connect(host, port, cb) {
    cb = cb || function(){}

    this.client = net.connect({host: host, port: port}, () => {
      cb(null)
    })
    this.client.on('data', (buffer) => {
      this.emit('data', buffer)
    })
    this.client.on('error', () => {})
  }

  write(buffer) {
    this.client.write(buffer)
  }

  close() {
    this.client.destroy()
  }
}

module.exports = TcpClient
